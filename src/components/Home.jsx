import React from "react";
import { CarouselCustomNavigation } from "./Carousel";
import Categories from "../features/categories/Categories";
import NewProducts from "../features/new/NewProducts";
import PopularProducts from "../features/popularProducts/PopularProducts";
import Advantages from "../features/advantages/Advantages";
import Team from "../features/team/Team";
import About from "../features/about/About";
import Brands from "../features/brands/Brands";

const Home = () => {
  return (
    <div className="container mb-10">
      <div className="h-[170px] md:h-[420px] lg:h-[420px] mt-5 lg:mt-14 ">
        <CarouselCustomNavigation />
      </div>
      <Categories />
      <NewProducts />
      <Brands />
      <PopularProducts />
      <Advantages />
      <Team />
      <About />
    </div>
  );
};

export default Home;
