import React from "react";
import SectionTitle from "../../ui/SectionTitile";
import ProductCard from "../../components/ProductCard";
import { useSelector } from "react-redux";

const BasketProductsList = () => {
  const { card } = useSelector((state) => state.card);
  return (
    <div className="container my-[76px]">
      <SectionTitle>Корзинка</SectionTitle>
      <div className="grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4">
        {card?.map((product, i) => {
          return <ProductCard key={product.id} product={product} />;
        })}
      </div>
    </div>
  );
};

export default BasketProductsList;
