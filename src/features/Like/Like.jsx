import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import ProductCard from "../../components/ProductCard";
import deleteCard from "../../assets/card/delete.png";

const Card = () => {
  const { card } = useSelector((state) => state.card);
  return (
    <div className="lg:container mx-auto px-4 mb-20">
      <h2 className=" text-[20px] font-bold mb-[30px] sm:text-[30px]">
        Избранные товары
      </h2>
      <div className=" grid grid-cols-1 xl:grid-cols-2 sm:grid-cols-4 gap-4">
        {card?.map((product, i) => {
          return (
            <div>
              <ProductCard key={product.id} product={product} />
              <img src={deleteCard} alt="" />
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Card;
