import React from "react";
import Phone1 from "../../assets/phone/clarity_mobile-phone-line.svg";
const Phone = () => {
  return (
    <div>
      <div className=" container ">
        <div className=" text-4xl">
          <h1>Контакты</h1>
        </div>
        <div className="flex">
          <div className="mt-[10px] w-auto ">
            <img src={Phone1} className="ml-[280px]" alt="" />
            <span className=" ml-[285px]">Телефон</span>
            <br />
            <span className=" ml-[246px]">+998 (94) 617-07-77</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Phone;
