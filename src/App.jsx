import React from "react";
import { RouterProvider, createBrowserRouter } from "react-router-dom";
import RootLayout from "./layout/RootLayout";
import Home from "./components/Home";
import AllNewProducts from "./features/new/AllNewProducts";
import PurchaseReturns from "./features/vozrost/PurchaseReturns";
import AllCategories from "./features/categories/AllCategories";
import AllPopularProducts from "./features/popularProducts/AllPopularProducts";
import Curer from "./features/Curecr/Curer";
import Phone from "./features/Curecr/Phone";
import Like from "./features/Like/Like";
import Card from "./features/Card/Card";

const App = () => {
  const router = createBrowserRouter([
    {
      path: "/",
      element: <RootLayout />,
      children: [
        {
          index: true,
          element: <Home />,
        },
        {
          path: "new",
          element: <AllNewProducts />,
        },
        {
          path: "card",
          element: <Card />,
        },

        {
          path: "categories",
          element: <AllCategories />,
        },

        {
          path: "popular",
          element: <AllPopularProducts />,
        },
        {
          path: "purchase/returns",
          element: <PurchaseReturns />,
        },
        {
          path: "curer",
          element: <Curer />,
        },
        {
          path: "phone",
          element: <Phone />,
        },
        {
          path: "card",
          element: <Card />,
        },
        {
          path: "like",
          element: <Like />,
        },
      ],
    },
  ]);
  return (
    <>
      <RouterProvider router={router} />
    </>
  );
};

export default App;
